package com.yosef.aplikasipemilihansmartphone.view.fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.yosef.aplikasipemilihansmartphone.R;
import com.yosef.aplikasipemilihansmartphone.base.BaseFragment;
import com.yosef.aplikasipemilihansmartphone.model.Komentar;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class KomentarFragment extends BaseFragment {

    @BindView(R.id.komentar_recycler) RecyclerView komentarRecycler;
    @BindView(R.id.refreshContainer) SwipeRefreshLayout refreshLayout;
    @BindView(R.id.edittext_komentar) EditText edittextKomentar;
    @BindView(R.id.button_komentar) Button buttonKomentar;
    DatabaseReference mDbKomentar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_komentar, container, false);
        ButterKnife.bind(this, rootView);

        mDbKomentar= FirebaseDatabase.getInstance().getReference().child("komentar");
        SwitchAdapter();

        refreshLayout.setOnRefreshListener(() -> {
            new Handler().postDelayed(() -> {
                SwitchAdapter();
                refreshLayout.setRefreshing(false);
            }, 1000);
        });

        buttonKomentar.setOnClickListener(view -> {
            if(!getText(edittextKomentar).equals("")){
                ProgressDialog pDialog = new ProgressDialog(getContext());
                pDialog.setMessage("Mengirim komentar");
                pDialog.show();
                String id_tipe = mDbKomentar.push().getKey();
                String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
                Calendar calander = Calendar.getInstance();
                SimpleDateFormat simpledateformat = new SimpleDateFormat("dd MMM yyyy");
                SimpleDateFormat simpledateformat2 = new SimpleDateFormat("HH:mm");

                mDbKomentar.child(id_tipe).child("teks").setValue(getText(edittextKomentar));
                mDbKomentar.child(id_tipe).child("uid_user").setValue(uid);
                mDbKomentar.child(id_tipe).child("uid").setValue(id_tipe);
                mDbKomentar.child(id_tipe).child("tanggal").setValue(simpledateformat.format(calander.getTime()));
                mDbKomentar.child(id_tipe).child("jam").setValue(simpledateformat2.format(calander.getTime()));
                final Handler handler = new Handler();
                handler.postDelayed(() -> {
                    pDialog.hide();
                    Toast.makeText(getContext(), "Komentar berhasil!", Toast.LENGTH_SHORT).show();
                    Fragment fragment = new KomentarFragment();
                    fragmentTransaction(fragment);
                }, 2000);
            }else {
                edittextKomentar.setError( "Komentar belum diisi!" );
            }
        });

        return rootView;
    }

    private void SwitchAdapter(){

        FirebaseRecyclerAdapter<Komentar, KomentarViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Komentar, KomentarViewHolder>(
                Komentar.class,
                R.layout.cardview_komentar,
                KomentarViewHolder.class,
                mDbKomentar
        ) {
            @Override
            protected void populateViewHolder(KomentarViewHolder holder, Komentar model, int position) {
                String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
                if(!uid.equals("Jhsl5utW8nXDfG9Cvp8X1XA7WRj1")) {
                    ((ViewManager) holder.komentarHapus.getParent()).removeView(holder.komentarHapus);
                }else{
                    holder.komentarHapus.setOnClickListener(v -> {
                        AlertDialog.Builder alertDialog  = new AlertDialog.Builder(getContext());
                        alertDialog.setTitle("Hapus Komentar")
                                .setCancelable(true)
                                .setMessage("Apakah anda yakin ingin menghapus komentar ini?")
                                .setPositiveButton("Ya", (dialog, which) -> {
                                    // continue with delete
                                    FirebaseDatabase.getInstance().getReference().child("komentar").child(model.getUid()).removeValue();
                                    notifyItemRemoved(position);
                                    Toast.makeText(getContext(), "Hapus komentar berhasil!", Toast.LENGTH_SHORT).show();
                                })
                                .setNegativeButton("Tidak", (dialog, which) -> {
                                    // do nothing
                                })
                                .setIcon(R.drawable.ic_warning)
                                .show();
                    });
                }
                DatabaseReference mDatabaseUser = FirebaseDatabase.getInstance().getReference().child("users").child(model.getUid_user());

                mDatabaseUser.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        holder.nama.setText(dataSnapshot.child("nama").getValue().toString());
                        holder.pekerjaan.setText(dataSnapshot.child("pekerjaan").getValue().toString());
                        if(dataSnapshot.child("jk").getValue().toString().equals("Pria")) {
                            holder.image.setImageDrawable(getResources().getDrawable(R.drawable.boy));
                        } else{
                            holder.image.setImageDrawable(getResources().getDrawable(R.drawable.girl));
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e("The read failed: " ,databaseError.getMessage());
                    }
                });
                holder.text.setText(model.getTeks());
                holder.tgl.setText(model.getTanggal());
                holder.jam.setText(model.getJam());
            }
        };
        LinearLayoutManager linearLayout = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        linearLayout.setStackFromEnd(true);
        komentarRecycler.setHasFixedSize(true);
        komentarRecycler.setLayoutManager(linearLayout);
        komentarRecycler.setDrawingCacheEnabled(true);
        komentarRecycler.setItemViewCacheSize(10);
        komentarRecycler.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        komentarRecycler.setAdapter(firebaseRecyclerAdapter);
    }

    public static class KomentarViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.komentar_image) ImageView image;
        @BindView(R.id.komentar_nama) TextView nama;
        @BindView(R.id.komentar_pekerjaan) TextView pekerjaan;
        @BindView(R.id.komentar_text) TextView text;
        @BindView(R.id.komentar_tgl) TextView tgl;
        @BindView(R.id.komentar_jam) TextView jam;
        @BindView(R.id.komentar_hapus) ImageView komentarHapus;

        public KomentarViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
