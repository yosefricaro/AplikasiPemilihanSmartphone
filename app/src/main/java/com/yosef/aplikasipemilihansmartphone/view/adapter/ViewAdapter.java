package com.yosef.aplikasipemilihansmartphone.view.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.yosef.aplikasipemilihansmartphone.R;
import com.yosef.aplikasipemilihansmartphone.model.Smartphone;
import com.yosef.aplikasipemilihansmartphone.model.StaticList;
import com.yosef.aplikasipemilihansmartphone.view.fragment.DetailFragment;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Yosefricaro on 18/11/2017.
 */

public class ViewAdapter extends RecyclerView.Adapter<ViewAdapter.ViewViewHolder> {

    private Context context;
    private List<Smartphone> smartphones;

    public ViewAdapter(List<Smartphone> smartphones) {
        this.smartphones = new ArrayList<>();
        this.smartphones.addAll(smartphones);
    }

    @Override
    public ViewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.cardview_view, parent, false);
        return new ViewViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewViewHolder holder, int position) {
        final Smartphone smartphone = smartphones.get(position);

        if (!TextUtils.isEmpty(smartphone.getImage_url()))
            Picasso.with(context)
                    .load(smartphone.getImage_url())
                    .placeholder(R.drawable.smartphone)
                    .error(R.drawable.smartphone)
                    .into(holder.image);

        holder.merk.setText(smartphone.getMerk());
        holder.tipe.setText(smartphone.getTipe());
        holder.harga.setText(smartphone.getHarga());
        holder.rilis.setText(smartphone.getRilis());
        holder.favorit.setText(smartphone.getFavorit() + " favorit");

        holder.itemView.setOnClickListener(v -> {
            StaticList.setSmartphone(smartphone);
            Fragment fragment = new DetailFragment();
            FragmentTransaction ft = ((AppCompatActivity)context).getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.addToBackStack(fragment.getClass().getName());
            ft.commit();
        });
    }

    @Override
    public int getItemCount() {
        return smartphones.size();
    }

    public static class ViewViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.smartphone_image) ImageView image;
        @BindView(R.id.smartphone_merk) TextView merk;
        @BindView(R.id.smartphone_tipe) TextView tipe;
        @BindView(R.id.smartphone_harga) TextView harga;
        @BindView(R.id.smartphone_rilis) TextView rilis;
        @BindView(R.id.smartphone_favorit) TextView favorit;

        public ViewViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
