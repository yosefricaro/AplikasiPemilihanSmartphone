package com.yosef.aplikasipemilihansmartphone.view.fragment;


import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.yosef.aplikasipemilihansmartphone.R;
import com.yosef.aplikasipemilihansmartphone.base.BaseFragment;
import com.yosef.aplikasipemilihansmartphone.model.Smartphone;
import com.yosef.aplikasipemilihansmartphone.model.StaticList;
import com.yosef.aplikasipemilihansmartphone.view.adapter.EditAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditSmartphoneFragment extends BaseFragment {

    @BindView(R.id.spinner_kriteria) Spinner spinnerKriteria;
    @BindView(R.id.edit_recycler) RecyclerView editRecycler;
    @BindView(R.id.refreshContainer) SwipeRefreshLayout refreshLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_edit_smartphone, container, false);
        ButterKnife.bind(this, rootView);

        spinnerKriteria.setAdapter(setDropdown(R.array.merk_option));

        spinnerKriteria.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SwitchSpinner();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}

        });

        refreshLayout.setOnRefreshListener(() -> {
            new Handler().postDelayed(() -> {
                SwitchSpinner();
                refreshLayout.setRefreshing(false);
            }, 1000);
        });

        return rootView;
    }

    private void SwitchSpinner(){
        try {
            switch (getItemSpinner(spinnerKriteria)) {
                case "Samsung":
                    SwitchAdapter(StaticList.getSamsung());
                    break;
                case "Oppo":
                    SwitchAdapter(StaticList.getOppo());
                    break;
                case "Advan":
                    SwitchAdapter(StaticList.getAdvan());
                    break;
                case "Asus":
                    SwitchAdapter(StaticList.getAsus());
                    break;
                case "Xiaomi":
                    SwitchAdapter(StaticList.getXiaomi());
                    break;
            }
        }catch (Exception e){
            Toast.makeText(getContext(), "Tidak ada internet", Toast.LENGTH_SHORT).show();
        }
    }

    private void SwitchAdapter(List<Smartphone> smartphones){
        EditAdapter editAdapter = new EditAdapter(smartphones);
        recyclerAdapter(editRecycler, editAdapter);
    }

//    private void SwitchAdapter(String merk){
//        DatabaseReference mDatabaseSmartphone = FirebaseDatabase.getInstance().getReference().child("smartphone").child(merk);
//
//        FirebaseRecyclerAdapter<Smartphone, EditViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Smartphone, EditViewHolder>(
//                Smartphone.class,
//                R.layout.cardview_edit,
//                EditViewHolder.class,
//                mDatabaseSmartphone
//        ) {
//            @Override
//            protected void populateViewHolder(EditViewHolder holder, Smartphone model, int position) {
//                if (!TextUtils.isEmpty(model.getImage_url()))
//                    Picasso.with(getContext())
//                            .load(model.getImage_url())
//                            .placeholder(R.drawable.smartphone)
//                            .error(R.drawable.smartphone)
//                            .into(holder.image);
//
//                holder.tipe.setText(model.getTipe());
//
//                holder.edit.setOnClickListener(v -> {
//                    StaticList.setSmartphone(model);
//                    Fragment fragment = new EditDetailFragment();
//                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
//                    ft.replace(R.id.content_frame, fragment);
//                    ft.addToBackStack(fragment.getClass().getName());
//                    ft.commit();
//                });
//
//                holder.delete.setOnClickListener(v -> {
//                    AlertDialog.Builder alertDialog  = new AlertDialog.Builder(getContext());
//                    alertDialog.setTitle("Hapus Smartphone")
//                            .setCancelable(true)
//                            .setMessage("Apakah anda yakin ingin menghapus smartphone " + model.getTipe() + " ini?")
//                            .setPositiveButton("Ya", (dialog, which) -> {
//                                // continue with delete
//                                FirebaseDatabase.getInstance().getReference().child("smartphone")
//                                        .child(model.getMerk().toLowerCase()).child(model.getUid()).removeValue();
//                            })
//                            .setNegativeButton("Tidak", (dialog, which) -> {
//                                // do nothing
//                            })
//                            .setIcon(R.drawable.ic_warning)
//                            .show();
//                });
//            }
//        };
//        recyclerAdapter(editRecycler, firebaseRecyclerAdapter);
//    }
//
//    public static class EditViewHolder extends RecyclerView.ViewHolder {
//        @BindView(R.id.smartphone_image) ImageView image;
//        @BindView(R.id.smartphone_tipe) TextView tipe;
//        @BindView(R.id.button_edit) ImageView edit;
//        @BindView(R.id.button_delete) ImageView delete;
//
//        public EditViewHolder(View itemView) {
//            super(itemView);
//            ButterKnife.bind(this, itemView);
//        }
//    }
}
