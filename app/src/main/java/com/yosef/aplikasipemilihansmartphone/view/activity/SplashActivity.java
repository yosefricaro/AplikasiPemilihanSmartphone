package com.yosef.aplikasipemilihansmartphone.view.activity;

import android.os.Bundle;
import android.os.Handler;

import com.google.firebase.auth.FirebaseAuth;
import com.yosef.aplikasipemilihansmartphone.R;
import com.yosef.aplikasipemilihansmartphone.base.BaseActivity;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        if(mAuth.getCurrentUser() == null){
            waitSplash(LoginActivity.class);
        }else {
            waitSplash(MainActivity.class);
        }
    }

    public void waitSplash(Class activity) {
        new Handler().postDelayed(() -> {
            openNewActivity(activity);
        }, 2000);
    }
}
