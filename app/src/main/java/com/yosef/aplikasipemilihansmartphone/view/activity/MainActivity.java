package com.yosef.aplikasipemilihansmartphone.view.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.onesignal.OneSignal;
import com.yosef.aplikasipemilihansmartphone.R;
import com.yosef.aplikasipemilihansmartphone.base.BaseActivity;
import com.yosef.aplikasipemilihansmartphone.model.Smartphone;
import com.yosef.aplikasipemilihansmartphone.model.StaticList;
import com.yosef.aplikasipemilihansmartphone.model.User;
import com.yosef.aplikasipemilihansmartphone.view.fragment.AboutFragment;
import com.yosef.aplikasipemilihansmartphone.view.fragment.AddFragment;
import com.yosef.aplikasipemilihansmartphone.view.fragment.EditProfilFragment;
import com.yosef.aplikasipemilihansmartphone.view.fragment.EditSmartphoneFragment;
import com.yosef.aplikasipemilihansmartphone.view.fragment.KomentarFragment;
import com.yosef.aplikasipemilihansmartphone.view.fragment.SearchFragment;
import com.yosef.aplikasipemilihansmartphone.view.fragment.ViewFragment;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        FirebaseAuth mAuth = FirebaseAuth.getInstance();

        // OneSignal Initialization
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
        try {
            OneSignal.sendTag("user_id", mAuth.getCurrentUser().getEmail());
        }catch (Exception e){}

        try {
            String user_id = mAuth.getCurrentUser().getUid();
            if(mAuth.getCurrentUser() != null){
                if(user_id.equals("Jhsl5utW8nXDfG9Cvp8X1XA7WRj1")){
                    navigationView.getMenu().setGroupVisible(R.id.nav_user, false);
                }else {
                    navigationView.getMenu().setGroupVisible(R.id.nav_admin, false);
                }
            } else {
                openNewActivity(LoginActivity.class);
            }

            DatabaseReference mUser = FirebaseDatabase.getInstance().getReference().child("users");

            mUser.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    StaticList.setUser(dataSnapshot.child(user_id).getValue(User.class));

                    View hView =  navigationView.getHeaderView(0);
                    TextView emailUser = hView.findViewById(R.id.user_email);
                    TextView namaUser = hView.findViewById(R.id.user_name);
                    ImageView imageView = hView.findViewById(R.id.imageAvatar);

                    emailUser.setText(StaticList.getUser().getEmail());
                    namaUser.setText(StaticList.getUser().getNama());
                    if(StaticList.getUser().getJk().equals("Pria")) {
                        imageView.setImageDrawable(getResources().getDrawable(R.drawable.boy));
                    } else{
                        imageView.setImageDrawable(getResources().getDrawable(R.drawable.girl));
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e("The read failed: " ,databaseError.getMessage());
                }
            });
        }catch (Exception e){}

        DatabaseReference mSmartphone = FirebaseDatabase.getInstance().getReference().child("smartphone");

        mSmartphone.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                StaticList.setSamsung(getDataSnapshot(dataSnapshot, "samsung"));
                StaticList.setOppo(getDataSnapshot(dataSnapshot, "oppo"));
                StaticList.setAsus(getDataSnapshot(dataSnapshot, "asus"));
                StaticList.setAdvan(getDataSnapshot(dataSnapshot, "advan"));
                StaticList.setXiaomi(getDataSnapshot(dataSnapshot, "xiaomi"));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("The read failed: " ,databaseError.getMessage());
            }
        });

        displaySelectedScreen(R.id.nav_search);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        //Checking for fragment count on backstack
        else if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else if (!doubleBackToExitPressedOnce) {
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this,"Silahkan tekan kembali untuk keluar", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
        } else {
            super.onBackPressed();
            return;
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        //calling the method displayselectedscreen and passing the id of selected menuvs
        displaySelectedScreen(item.getItemId());
        return true;
    }

    public void displaySelectedScreen(int itemId) {

        //creating fragment object
        Fragment fragment = null;
        FragmentManager fm = getSupportFragmentManager();
        fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        //initializing the fragment object which is selected
        switch (itemId) {
            case R.id.nav_search_admin:
            case R.id.nav_search:
                fragment = new SearchFragment();
                break;
            case R.id.nav_add:
                fragment = new AddFragment();
                break;
            case R.id.nav_view_admin:
            case R.id.nav_view:
                fragment = new ViewFragment();
                break;
            case R.id.nav_edit_p:
                fragment = new EditProfilFragment();
                break;
            case R.id.nav_edit_s:
                fragment = new EditSmartphoneFragment();
                break;
            case R.id.nav_komentar_admin:
            case R.id.nav_komentar:
                fragment = new KomentarFragment();
                break;
            case R.id.nav_about:
                fragment = new AboutFragment();
                break;
            case R.id.nav_signout_admin:
            case R.id.nav_signout:
                FirebaseAuth mAuth = FirebaseAuth.getInstance();
                mAuth.signOut();
                openNewActivity(LoginActivity.class);
                break;
        }

        //replacing the fragment
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    private List<Smartphone> getDataSnapshot(DataSnapshot dataSnapshot, String merk){
        List<Smartphone> lSmartphone = new ArrayList<>();
        for (DataSnapshot child: dataSnapshot.child(merk).getChildren()) {
            lSmartphone.add(child.getValue(Smartphone.class));
        }
        return lSmartphone;
    }
}
