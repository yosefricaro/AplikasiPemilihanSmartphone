package com.yosef.aplikasipemilihansmartphone.view.fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.yosef.aplikasipemilihansmartphone.R;
import com.yosef.aplikasipemilihansmartphone.base.BaseFragment;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddFragment extends BaseFragment {

    @BindView(R.id.spinner_merk) Spinner spinnerMerk;
    @BindView(R.id.edittext_tipe) EditText edittextTipe;
    @BindView(R.id.image_gallery) ImageView imageGallery;
    @BindView(R.id.spinner_harga) Spinner spinnerHarga;
    @BindView(R.id.spinner_layar) Spinner spinnerLayar;
    @BindView(R.id.spinner_processor) Spinner spinnerProcessor;
    @BindView(R.id.spinner_ram) Spinner spinnerRAM;
    @BindView(R.id.spinner_memori) Spinner spinnerMemori;
    @BindView(R.id.spinner_kamerablk) Spinner spinnerKamerablk;
    @BindView(R.id.spinner_kameradpn) Spinner spinnerKameradpn;
    @BindView(R.id.spinner_baterai) Spinner spinnerBaterai;
    @BindView(R.id.edittext_harga) EditText edittextHarga;
    @BindView(R.id.edittext_rilis) EditText edittextRilis;
    @BindView(R.id.edittext_layar) EditText edittextLayar;
    @BindView(R.id.edittext_os) EditText edittextOS;
    @BindView(R.id.edittext_chipset) EditText edittextChipset;
    @BindView(R.id.edittext_processor) EditText edittextProcessor;
    @BindView(R.id.edittext_memori) EditText edittextMemori;
    @BindView(R.id.edittext_ram) EditText edittextRAM;
    @BindView(R.id.edittext_kamerablk) EditText edittextKamerablk;
    @BindView(R.id.edittext_kameradpn) EditText edittextKameradpn;
    @BindView(R.id.edittext_baterai) EditText edittextBaterai;
    @BindView(R.id.button_save) Button buttonSave;
    @BindView(R.id.button_reset) Button buttonReset;

    Uri file;
    static final int REQUEST_IMAGE_GET = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_add, container, false);
        ButterKnife.bind(this, rootView);

        spinnerMerk.setAdapter(setDropdown(R.array.merk_option));
        spinnerHarga.setAdapter(setDropdown(R.array.harga_option));
        spinnerLayar.setAdapter(setDropdown(R.array.layar_option));
        spinnerProcessor.setAdapter(setDropdown(R.array.processor_option));
        spinnerRAM.setAdapter(setDropdown(R.array.ram_option));
        spinnerMemori.setAdapter(setDropdown(R.array.memori_option));
        spinnerKamerablk.setAdapter(setDropdown(R.array.kamera_option));
        spinnerKameradpn.setAdapter(setDropdown(R.array.kamera_option));
        spinnerBaterai.setAdapter(setDropdown(R.array.baterai_option));

        imageGallery.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivityForResult(intent, REQUEST_IMAGE_GET);
            }
        });

        buttonSave.setOnClickListener(v -> {
            if( edittextTipe.getText().toString().trim().equals("")){
                edittextTipe.setError( "Tipe belum diisi!" );
            }else {
                ProgressDialog pDialog = new ProgressDialog(getContext());
                pDialog.setMessage("Mengirim data");
                pDialog.show();

                DatabaseReference mDbSmartphone = FirebaseDatabase.getInstance().getReference()
                        .child("smartphone").child(getItemSpinner(spinnerMerk).toLowerCase());
                String id_tipe = mDbSmartphone.push().getKey();
                mDbSmartphone.child(id_tipe).child("uid").setValue(id_tipe);
                mDbSmartphone.child(id_tipe).child("merk").setValue(getItemSpinner(spinnerMerk));
                mDbSmartphone.child(id_tipe).child("tipe").setValue(getText(edittextTipe));

                if(file != null) {
                    StorageReference mStorageRef = FirebaseStorage.getInstance()
                            .getReference().child("images/" + getText(edittextTipe));
                    mStorageRef.putFile(file)
                            .addOnSuccessListener(taskSnapshot -> {
                                // Get a URL to the uploaded content
                                @SuppressWarnings("VisibleForTests") String url = taskSnapshot.getDownloadUrl().toString();
                                mDbSmartphone.child(id_tipe).child("image_url").setValue(url);
                            })
                            .addOnFailureListener(exception -> {
                                // Handle unsuccessful uploads
                            });
                } else{
                    mDbSmartphone.child(id_tipe).child("image_url").setValue("https://firebasestorage.googleapis.com/v0/b/aplikasipemilihansmartphone.appspot.com/o/images%2FNoImage.png?alt=media&token=bb23eb37-e8d6-40e2-8eb2-e8dfec407073");
                }

                mDbSmartphone.child(id_tipe).child("bobot_harga").setValue(getPositionSpinner(spinnerHarga));
                mDbSmartphone.child(id_tipe).child("bobot_layar").setValue(getPositionSpinner(spinnerLayar));
                int position = getPositionSpinner(spinnerProcessor);
                if (position == 1) {
                    mDbSmartphone.child(id_tipe).child("bobot_processor").setValue(1);
                } else if (position == 2) {
                    mDbSmartphone.child(id_tipe).child("bobot_processor").setValue(3);
                } else {
                    mDbSmartphone.child(id_tipe).child("bobot_processor").setValue(5);
                }
                mDbSmartphone.child(id_tipe).child("bobot_ram").setValue(getPositionSpinner(spinnerRAM));
                mDbSmartphone.child(id_tipe).child("bobot_memori").setValue(getPositionSpinner(spinnerMemori));
                mDbSmartphone.child(id_tipe).child("bobot_kamerablk").setValue(getPositionSpinner(spinnerKamerablk));
                mDbSmartphone.child(id_tipe).child("bobot_kameradpn").setValue(getPositionSpinner(spinnerKameradpn));
                mDbSmartphone.child(id_tipe).child("bobot_baterai").setValue(getPositionSpinner(spinnerBaterai));
                mDbSmartphone.child(id_tipe).child("harga").setValue(getText(edittextHarga));
                mDbSmartphone.child(id_tipe).child("rilis").setValue(getText(edittextRilis));
                mDbSmartphone.child(id_tipe).child("layar").setValue(getText(edittextLayar));
                mDbSmartphone.child(id_tipe).child("os").setValue(getText(edittextOS));
                mDbSmartphone.child(id_tipe).child("chipset").setValue(getText(edittextChipset));
                mDbSmartphone.child(id_tipe).child("processor").setValue(getText(edittextProcessor));
                mDbSmartphone.child(id_tipe).child("ram").setValue(getText(edittextRAM));
                mDbSmartphone.child(id_tipe).child("memori").setValue(getText(edittextMemori));
                mDbSmartphone.child(id_tipe).child("kamerablk").setValue(getText(edittextKamerablk));
                mDbSmartphone.child(id_tipe).child("kameradpn").setValue(getText(edittextKameradpn));
                mDbSmartphone.child(id_tipe).child("baterai").setValue(getText(edittextBaterai));
                mDbSmartphone.child(id_tipe).child("favorit").setValue(0);

                new SendNotifTask().execute(getItemSpinner(spinnerMerk) + " " + getText(edittextTipe));

                final Handler handler = new Handler();
                handler.postDelayed(() -> {
                    pDialog.hide();
                    Toast.makeText(getContext(), "Tambah smartphone berhasil!", Toast.LENGTH_SHORT).show();
                    Fragment fragment = new AddFragment();
                    fragmentTransaction(fragment);
                }, 2000);
            }
        });

        buttonReset.setOnClickListener(v -> {
            Fragment fragment = new AddFragment();
            fragmentTransaction(fragment);
        });

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_GET && resultCode == RESULT_OK) {
            file = data.getData();
            imageGallery.setImageURI(file);
        }
    }

    private static class SendNotifTask extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... tipe) {
            try {
                String jsonResponse;

                URL url = new URL("https://onesignal.com/api/v1/notifications");
                HttpURLConnection con = (HttpURLConnection)url.openConnection();
                con.setUseCaches(false);
                con.setDoOutput(true);
                con.setDoInput(true);

                con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                con.setRequestProperty("Authorization", "Basic YWMzMjcwMjItZWZiYi00Y2EzLTlhZjgtZjdlN2M1M2IwMzk5");
                con.setRequestMethod("POST");

                String strJsonBody = "{"
                        +   "\"app_id\": \"691947f8-4b24-411b-800e-6ebcaa6295b3\","
                        +   "\"included_segments\": [\"All\"],"
                        +   "\"data\": {\"foo\": \"bar\"},"
                        +   "\"contents\": {\"en\": \"Lihat spesifikasi " + tipe[0] + " sekarang juga!\"}"
                        + "}";


                System.out.println("strJsonBody:\n" + strJsonBody);

                byte[] sendBytes = strJsonBody.getBytes("UTF-8");
                con.setFixedLengthStreamingMode(sendBytes.length);

                OutputStream outputStream = con.getOutputStream();
                outputStream.write(sendBytes);

                int httpResponse = con.getResponseCode();
                System.out.println("httpResponse: " + httpResponse);

                if (  httpResponse >= HttpURLConnection.HTTP_OK
                        && httpResponse < HttpURLConnection.HTTP_BAD_REQUEST) {
                    Scanner scanner = new Scanner(con.getInputStream(), "UTF-8");
                    jsonResponse = scanner.useDelimiter("\\A").hasNext() ? scanner.next() : "";
                    scanner.close();
                }
                else {
                    Scanner scanner = new Scanner(con.getErrorStream(), "UTF-8");
                    jsonResponse = scanner.useDelimiter("\\A").hasNext() ? scanner.next() : "";
                    scanner.close();
                }
                System.out.println("jsonResponse:\n" + jsonResponse);

            } catch(Throwable t) {
                t.printStackTrace();
            }
            return null;
        }
    }
}
