package com.yosef.aplikasipemilihansmartphone.view.fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;
import com.yosef.aplikasipemilihansmartphone.R;
import com.yosef.aplikasipemilihansmartphone.base.BaseFragment;
import com.yosef.aplikasipemilihansmartphone.model.Smartphone;
import com.yosef.aplikasipemilihansmartphone.model.StaticList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditDetailFragment extends BaseFragment {
    
    @BindView(R.id.edittext_tipe) EditText edittextTipe;
    @BindView(R.id.image_gallery) ImageView imageGallery;
    @BindView(R.id.spinner_harga) Spinner spinnerHarga;
    @BindView(R.id.spinner_layar) Spinner spinnerLayar;
    @BindView(R.id.spinner_processor) Spinner spinnerProcessor;
    @BindView(R.id.spinner_ram) Spinner spinnerRAM;
    @BindView(R.id.spinner_memori) Spinner spinnerMemori;
    @BindView(R.id.spinner_kamerablk) Spinner spinnerKamerablk;
    @BindView(R.id.spinner_kameradpn) Spinner spinnerKameradpn;
    @BindView(R.id.spinner_baterai) Spinner spinnerBaterai;
    @BindView(R.id.edittext_harga) EditText edittextHarga;
    @BindView(R.id.edittext_rilis) EditText edittextRilis;
    @BindView(R.id.edittext_layar) EditText edittextLayar;
    @BindView(R.id.edittext_os) EditText edittextOS;
    @BindView(R.id.edittext_chipset) EditText edittextChipset;
    @BindView(R.id.edittext_processor) EditText edittextProcessor;
    @BindView(R.id.edittext_memori) EditText edittextMemori;
    @BindView(R.id.edittext_ram) EditText edittextRAM;
    @BindView(R.id.edittext_kamerablk) EditText edittextKamerablk;
    @BindView(R.id.edittext_kameradpn) EditText edittextKameradpn;
    @BindView(R.id.edittext_baterai) EditText edittextBaterai;
    @BindView(R.id.button_edit) Button buttonEdit;
    @BindView(R.id.button_cancel) Button buttonCancel;

    Uri file;
    static final int REQUEST_IMAGE_GET = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_edit_detail, container, false);
        ButterKnife.bind(this, rootView);

        Smartphone smartphone = StaticList.getSmartphone();
        
        spinnerHarga.setAdapter(setDropdown(R.array.harga_option));
        spinnerLayar.setAdapter(setDropdown(R.array.layar_option));
        spinnerProcessor.setAdapter(setDropdown(R.array.processor_option));
        spinnerRAM.setAdapter(setDropdown(R.array.ram_option));
        spinnerMemori.setAdapter(setDropdown(R.array.memori_option));
        spinnerKamerablk.setAdapter(setDropdown(R.array.kamera_option));
        spinnerKameradpn.setAdapter(setDropdown(R.array.kamera_option));
        spinnerBaterai.setAdapter(setDropdown(R.array.baterai_option));
        
        edittextTipe.setText(smartphone.getTipe());
        if (!TextUtils.isEmpty(smartphone.getImage_url()))
            Picasso.with(getContext())
                    .load(smartphone.getImage_url())
                    .placeholder(R.drawable.smartphone)
                    .error(R.drawable.smartphone)
                    .into(imageGallery);
        spinnerHarga.setSelection(smartphone.getBobot_harga()-1);
        spinnerLayar.setSelection(smartphone.getBobot_layar()-1);
        if (smartphone.getBobot_processor() == 1) {
            spinnerProcessor.setSelection(0);
        } else if (smartphone.getBobot_processor() == 3) {
            spinnerProcessor.setSelection(1);
        } else {
            spinnerProcessor.setSelection(2);
        }
        spinnerRAM.setSelection(smartphone.getBobot_ram()-1);
        spinnerMemori.setSelection(smartphone.getBobot_memori()-1);
        spinnerKamerablk.setSelection(smartphone.getBobot_kamerablk()-1);
        spinnerKameradpn.setSelection(smartphone.getBobot_kameradpn()-1);
        spinnerBaterai.setSelection(smartphone.getBobot_baterai()-1);
        edittextHarga.setText(smartphone.getHarga());
        edittextRilis.setText(smartphone.getRilis());
        edittextLayar.setText(smartphone.getLayar());
        edittextOS.setText(smartphone.getOs());
        edittextChipset.setText(smartphone.getChipset());
        edittextProcessor.setText(smartphone.getProcessor());
        edittextRAM.setText(smartphone.getRam());
        edittextMemori.setText(smartphone.getMemori());
        edittextKamerablk.setText(smartphone.getKamerablk());
        edittextKameradpn.setText(smartphone.getKameradpn());
        edittextBaterai.setText(smartphone.getBaterai());

        imageGallery.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivityForResult(intent, REQUEST_IMAGE_GET);
            }
        });

        buttonEdit.setOnClickListener(v -> {
            if( edittextTipe.getText().toString().trim().equals("")){
                edittextTipe.setError( "Tipe belum diisi!" );
            }else {
                ProgressDialog pDialog = new ProgressDialog(getContext());
                pDialog.setMessage("Mengirim data");
                pDialog.show();

                DatabaseReference mDbSmartphone = FirebaseDatabase.getInstance().getReference()
                        .child("smartphone").child(smartphone.getMerk().toLowerCase()).child(smartphone.getUid());
                mDbSmartphone.child("tipe").setValue(getText(edittextTipe));

                StorageReference mStorageRef = FirebaseStorage.getInstance()
                        .getReference().child("images/");

                if(file != null) {
                    mStorageRef.child(getText(edittextTipe)).putFile(file)
                            .addOnSuccessListener(taskSnapshot -> {
                                // Get a URL to the uploaded content
                                @SuppressWarnings("VisibleForTests") String url = taskSnapshot.getDownloadUrl().toString();
                                mDbSmartphone.child("image_url").setValue(url);
                            })
                            .addOnFailureListener(exception -> {
                                // Handle unsuccessful uploads
                            });
                    if(!getText(edittextTipe).equals(smartphone.getTipe())){
                        mStorageRef.child(smartphone.getTipe()).delete();
                    }
                }

                mDbSmartphone.child("bobot_harga").setValue(getPositionSpinner(spinnerHarga));
                mDbSmartphone.child("bobot_layar").setValue(getPositionSpinner(spinnerLayar));
                int position = getPositionSpinner(spinnerProcessor);
                if (position == 1) {
                    mDbSmartphone.child("bobot_processor").setValue(1);
                } else if (position == 2) {
                    mDbSmartphone.child("bobot_processor").setValue(3);
                } else {
                    mDbSmartphone.child("bobot_processor").setValue(5);
                }
                mDbSmartphone.child("bobot_ram").setValue(getPositionSpinner(spinnerRAM));
                mDbSmartphone.child("bobot_memori").setValue(getPositionSpinner(spinnerMemori));
                mDbSmartphone.child("bobot_kamerablk").setValue(getPositionSpinner(spinnerKamerablk));
                mDbSmartphone.child("bobot_kameradpn").setValue(getPositionSpinner(spinnerKameradpn));
                mDbSmartphone.child("bobot_baterai").setValue(getPositionSpinner(spinnerBaterai));
                mDbSmartphone.child("harga").setValue(getText(edittextHarga));
                mDbSmartphone.child("rilis").setValue(getText(edittextRilis));
                mDbSmartphone.child("layar").setValue(getText(edittextLayar));
                mDbSmartphone.child("os").setValue(getText(edittextOS));
                mDbSmartphone.child("chipset").setValue(getText(edittextChipset));
                mDbSmartphone.child("processor").setValue(getText(edittextProcessor));
                mDbSmartphone.child("ram").setValue(getText(edittextRAM));
                mDbSmartphone.child("memori").setValue(getText(edittextMemori));
                mDbSmartphone.child("kamerablk").setValue(getText(edittextKamerablk));
                mDbSmartphone.child("kameradpn").setValue(getText(edittextKameradpn));
                mDbSmartphone.child("baterai").setValue(getText(edittextBaterai));
                final Handler handler = new Handler();
                handler.postDelayed(() -> {
                    pDialog.hide();
                    Toast.makeText(getContext(), "Ubah smartphone berhasil!", Toast.LENGTH_SHORT).show();
                    Fragment fragment = new EditSmartphoneFragment();
                    fragmentTransaction(fragment);
                }, 2000);
            }
        });

        buttonCancel.setOnClickListener(v -> {
            getActivity().getSupportFragmentManager().popBackStack();
        });

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_GET && resultCode == RESULT_OK) {
            file = data.getData();
            imageGallery.setImageURI(file);
        }
    }
}
