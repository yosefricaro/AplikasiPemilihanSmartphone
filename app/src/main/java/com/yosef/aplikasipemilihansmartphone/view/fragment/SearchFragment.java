package com.yosef.aplikasipemilihansmartphone.view.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.yosef.aplikasipemilihansmartphone.R;
import com.yosef.aplikasipemilihansmartphone.base.BaseFragment;
import com.yosef.aplikasipemilihansmartphone.model.Smartphone;
import com.yosef.aplikasipemilihansmartphone.model.StaticList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends BaseFragment {

    @BindView(R.id.spinner_harga) Spinner spinnerHarga;
    @BindView(R.id.spinner_layar) Spinner spinnerLayar;
    @BindView(R.id.spinner_processor) Spinner spinnerProcessor;
    @BindView(R.id.spinner_ram) Spinner spinnerRAM;
    @BindView(R.id.spinner_memori) Spinner spinnerMemori;
    @BindView(R.id.spinner_kamerablk) Spinner spinnerKamerablk;
    @BindView(R.id.spinner_kameradpn) Spinner spinnerKameradpn;
    @BindView(R.id.spinner_baterai) Spinner spinnerBaterai;
    @BindView(R.id.button_save) Button buttonSave;
    @BindView(R.id.button_reset) Button buttonReset;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.bind(this, rootView);

        spinnerHarga.setAdapter(setDropdown(R.array.harga_option));
        spinnerLayar.setAdapter(setDropdown(R.array.bobot));
        spinnerProcessor.setAdapter(setDropdown(R.array.bobot));
        spinnerRAM.setAdapter(setDropdown(R.array.bobot));
        spinnerMemori.setAdapter(setDropdown(R.array.bobot));
        spinnerKamerablk.setAdapter(setDropdown(R.array.bobot));
        spinnerKameradpn.setAdapter(setDropdown(R.array.bobot));
        spinnerBaterai.setAdapter(setDropdown(R.array.bobot));

        buttonSave.setOnClickListener(v -> {
            ProgressDialog pDialog = new ProgressDialog(getContext());
            pDialog.setMessage("Mengirim data");
            pDialog.show();

            int position = getPositionSpinner(spinnerProcessor);
            if(position == 2){ position = 3; }
            else if(position == 3){ position = 5; }

            double[] bobot = {
                    getPositionSpinner(spinnerLayar),
                    position,
                    getPositionSpinner(spinnerRAM),
                    getPositionSpinner(spinnerMemori),
                    getPositionSpinner(spinnerKamerablk),
                    getPositionSpinner(spinnerKameradpn),
                    getPositionSpinner(spinnerBaterai),
            };

            double jumlahBobot = 0;
            for (double b : bobot){
                jumlahBobot = jumlahBobot + b;
            }

            double[] fixBobot = {
                    bobot[0]/jumlahBobot,
                    bobot[1]/jumlahBobot,
                    bobot[2]/jumlahBobot,
                    bobot[3]/jumlahBobot,
                    bobot[4]/jumlahBobot,
                    bobot[5]/jumlahBobot,
                    bobot[6]/jumlahBobot
            };

            List<Smartphone> smartphones = new ArrayList<>();
            double jumlahVektorS = 0;
            int harga = getPositionSpinner(spinnerHarga);
            try {
                jumlahVektorS = jumlahVektorS + hitungVektorS(smartphones, StaticList.getSamsung(), fixBobot, harga);
                jumlahVektorS = jumlahVektorS + hitungVektorS(smartphones, StaticList.getOppo(), fixBobot, harga);
                jumlahVektorS = jumlahVektorS + hitungVektorS(smartphones, StaticList.getAdvan(), fixBobot, harga);
                jumlahVektorS = jumlahVektorS + hitungVektorS(smartphones, StaticList.getAsus(), fixBobot, harga);
                jumlahVektorS = jumlahVektorS + hitungVektorS(smartphones, StaticList.getXiaomi(), fixBobot, harga);

                for(Smartphone smartphone : smartphones){
                    smartphone.setVektorV(Double.parseDouble(String.format("%.4f",smartphone.getVektorS() / jumlahVektorS)));
                }

                selectionSort(smartphones);
                Collections.reverse(smartphones);

                final Handler handler = new Handler();
                handler.postDelayed(() -> {
                    StaticList.setResult(smartphones);
                    pDialog.dismiss();
                    Fragment fragment = new ResultFragment();
                    FragmentTransaction ft = fragmentTransaction(fragment);
                    ft.addToBackStack(fragment.getClass().getName());
                }, 2000);

            }catch (Exception e){
                pDialog.dismiss();
                Toast.makeText(getContext(), "Tidak ada internet", Toast.LENGTH_SHORT).show();
            }
        });

        buttonReset.setOnClickListener(v -> {
            Fragment fragment = new SearchFragment();
            fragmentTransaction(fragment);
        });

        return rootView;
    }

    private double hitungVektorS(List<Smartphone> smartphones, List<Smartphone> merk, double[] fixBobot, int harga){
        double jumlahVektorS = 0;
        for(Smartphone smartphone : merk){
            if(harga == smartphone.getBobot_harga()) {
                double vs = Math.pow(smartphone.getBobot_layar(), fixBobot[0])
                        * Math.pow(smartphone.getBobot_processor(), fixBobot[1])
                        * Math.pow(smartphone.getBobot_ram(), fixBobot[2])
                        * Math.pow(smartphone.getBobot_memori(), fixBobot[3])
                        * Math.pow(smartphone.getBobot_kamerablk(), fixBobot[4])
                        * Math.pow(smartphone.getBobot_kameradpn(), fixBobot[5])
                        * Math.pow(smartphone.getBobot_baterai(), fixBobot[6]);
                smartphone.setVektorS(vs);
                smartphones.add(smartphone);
                jumlahVektorS = jumlahVektorS + vs;
            }
        }
        return jumlahVektorS;
    }

    private void selectionSort(List<Smartphone> smartphones){
        for (int i = 0; i < smartphones.size() - 1; i++)
        {
            int index = i;
            for (int j = i + 1; j < smartphones.size(); j++){
                if (smartphones.get(j).getVektorV() < smartphones.get(index).getVektorV()){
                    index = j;//searching for lowest index
                }
            }
            Smartphone smallerNumber = smartphones.get(index);
            smartphones.set(index, smartphones.get(i));
            smartphones.set(i, smallerNumber);
        }
    }
}