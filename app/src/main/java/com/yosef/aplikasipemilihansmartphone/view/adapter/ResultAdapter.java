package com.yosef.aplikasipemilihansmartphone.view.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.yosef.aplikasipemilihansmartphone.R;
import com.yosef.aplikasipemilihansmartphone.model.Smartphone;
import com.yosef.aplikasipemilihansmartphone.model.StaticList;
import com.yosef.aplikasipemilihansmartphone.view.fragment.DetailFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Yosefricaro on 17/11/2017.
 */

public class ResultAdapter extends RecyclerView.Adapter<ResultAdapter.SearchViewHolder> {

    private Context context;
    private List<Smartphone> smartphones;

    public ResultAdapter(List<Smartphone> smartphones){
        this.smartphones = new ArrayList<>();
        this.smartphones.addAll(smartphones);
    }

    @Override
    public SearchViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.cardview_result, parent, false);
        return new SearchViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SearchViewHolder holder, int position) {
        final Smartphone smartphone = smartphones.get(position);

        if (!TextUtils.isEmpty(smartphone.getImage_url()))
            Picasso.with(context)
                    .load(smartphone.getImage_url())
                    .placeholder(R.drawable.smartphone)
                    .error(R.drawable.smartphone)
                    .into(holder.image);

        int peringkat = position+1;
        holder.value.setText("Rekomendasi " + peringkat + " (" + smartphone.getVektorV() + ")");
        holder.merk.setText(smartphone.getMerk());
        holder.tipe.setText(smartphone.getTipe());
        holder.harga.setText(smartphone.getHarga());

        DatabaseReference mDatabaseSmartphone = FirebaseDatabase.getInstance().getReference().child("smartphone")
                .child(smartphone.getMerk().toLowerCase()).child(smartphone.getUid());

        mDatabaseSmartphone.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                holder.favorit.setText(dataSnapshot.child("favorit").getValue() + " favorit");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("The read failed: ", databaseError.getMessage());
            }
        });

        holder.itemView.setOnClickListener(v -> {
            StaticList.setSmartphone(smartphone);
            Fragment fragment = new DetailFragment();
            FragmentTransaction ft = ((AppCompatActivity)context).getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.addToBackStack(fragment.getClass().getName());
            ft.commit();
        });
    }

    @Override
    public int getItemCount() {
        return smartphones.size();
    }

    public static class SearchViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.smartphone_image) ImageView image;
        @BindView(R.id.smartphone_value) TextView value;
        @BindView(R.id.smartphone_merk) TextView merk;
        @BindView(R.id.smartphone_tipe) TextView tipe;
        @BindView(R.id.smartphone_harga) TextView harga;
        @BindView(R.id.smartphone_favorit) TextView favorit;

        public SearchViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}