package com.yosef.aplikasipemilihansmartphone.view.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;
import com.yosef.aplikasipemilihansmartphone.R;
import com.yosef.aplikasipemilihansmartphone.model.Smartphone;
import com.yosef.aplikasipemilihansmartphone.model.StaticList;
import com.yosef.aplikasipemilihansmartphone.view.fragment.EditDetailFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Yosefricaro on 18/11/2017.
 */

public class EditAdapter extends RecyclerView.Adapter<EditAdapter.EditViewHolder> {

    private Context context;
    private List<Smartphone> smartphones;

    public EditAdapter(List<Smartphone> smartphones) {
        this.smartphones = new ArrayList<>();
        this.smartphones.addAll(smartphones);
    }

    @Override
    public EditViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.cardview_edit, parent, false);
        return new EditViewHolder(view);
    }

    @Override
    public void onBindViewHolder(EditViewHolder holder, int position) {
        final Smartphone smartphone = smartphones.get(position);

        if (!TextUtils.isEmpty(smartphone.getImage_url()))
            Picasso.with(context)
                    .load(smartphone.getImage_url())
                    .placeholder(R.drawable.smartphone)
                    .error(R.drawable.smartphone)
                    .into(holder.image);

        holder.tipe.setText(smartphone.getTipe());

        holder.edit.setOnClickListener(v -> {
            StaticList.setSmartphone(smartphone);
            Fragment fragment = new EditDetailFragment();
            FragmentTransaction ft = ((AppCompatActivity)context).getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.addToBackStack(fragment.getClass().getName());
            ft.commit();
        });

        holder.delete.setOnClickListener(v -> {
            AlertDialog.Builder alertDialog  = new AlertDialog.Builder(context);
            alertDialog.setTitle("Hapus Smartphone")
                    .setCancelable(true)
                    .setMessage("Apakah anda yakin ingin menghapus smartphone " + smartphone.getMerk() + " " + smartphone.getTipe() + "?")
                    .setPositiveButton("Ya", (dialog, which) -> {
                        // continue with delete
                        FirebaseDatabase.getInstance().getReference().child("smartphone")
                                .child(smartphone.getMerk().toLowerCase()).child(smartphone.getUid()).removeValue();
                        smartphones.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, smartphones.size());
                        Toast.makeText(context, "Hapus smartphone berhasil!", Toast.LENGTH_SHORT).show();
                    })
                    .setNegativeButton("Tidak", (dialog, which) -> {
                        // do nothing
                    })
                    .setIcon(R.drawable.ic_warning)
                    .show();
        });
    }

    @Override
    public int getItemCount() {
        return smartphones.size();
    }

    public static class EditViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.smartphone_image) ImageView image;
        @BindView(R.id.smartphone_tipe) TextView tipe;
        @BindView(R.id.button_edit) ImageView edit;
        @BindView(R.id.button_delete) ImageView delete;

        public EditViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
