package com.yosef.aplikasipemilihansmartphone.view.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yosef.aplikasipemilihansmartphone.R;
import com.yosef.aplikasipemilihansmartphone.base.BaseFragment;
import com.yosef.aplikasipemilihansmartphone.model.StaticList;
import com.yosef.aplikasipemilihansmartphone.view.adapter.ResultAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class ResultFragment extends BaseFragment {

    @BindView(R.id.result_recycler) RecyclerView resultRecycler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_result, container, false);
        ButterKnife.bind(this, rootView);

        ResultAdapter resultAdapter = new ResultAdapter(StaticList.getResult());
        recyclerAdapter(resultRecycler, resultAdapter);

        return rootView;
    }

}
