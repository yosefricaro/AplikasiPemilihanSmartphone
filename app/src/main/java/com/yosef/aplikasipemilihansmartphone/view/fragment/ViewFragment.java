package com.yosef.aplikasipemilihansmartphone.view.fragment;


import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.yosef.aplikasipemilihansmartphone.R;
import com.yosef.aplikasipemilihansmartphone.base.BaseFragment;
import com.yosef.aplikasipemilihansmartphone.model.Smartphone;
import com.yosef.aplikasipemilihansmartphone.model.StaticList;
import com.yosef.aplikasipemilihansmartphone.view.adapter.ViewAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class ViewFragment extends BaseFragment {

    @BindView(R.id.spinner_sortby) Spinner spinnerSortby;
    @BindView(R.id.spinner_kriteria) Spinner spinnerKriteria;
    @BindView(R.id.view_recycler) RecyclerView viewRecycler;
    @BindView(R.id.refreshContainer) SwipeRefreshLayout refreshLayout;

    List<Smartphone> ls;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_view, container, false);
        ButterKnife.bind(this, rootView);

        spinnerSortby.setAdapter(setDropdown(R.array.sortby));

        spinnerSortby.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(getItemSpinner(spinnerSortby).equals("Merk"))
                    spinnerKriteria.setAdapter(setDropdown(R.array.merk_option));
                else spinnerKriteria.setAdapter(setDropdown(R.array.harga_option));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}

        });

        spinnerKriteria.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SwitchSpinner();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}

        });

        refreshLayout.setOnRefreshListener(() -> {
            new Handler().postDelayed(() -> {
                SwitchSpinner();
                refreshLayout.setRefreshing(false);
            }, 1000);
        });

        return rootView;
    }

    private void SwitchSpinner(){
        try {
            switch (getItemSpinner(spinnerKriteria)) {
                case "Samsung":
                    SwitchAdapter(StaticList.getSamsung());
                    break;
                case "Oppo":
                    SwitchAdapter(StaticList.getOppo());
                    break;
                case "Advan":
                    SwitchAdapter(StaticList.getAdvan());
                    break;
                case "Asus":
                    SwitchAdapter(StaticList.getAsus());
                    break;
                case "Xiaomi":
                    SwitchAdapter(StaticList.getXiaomi());
                    break;
                case "<1 juta":
                case "1 – <2 juta":
                case "2 – <3 juta":
                case "3 – <4 juta":
                case ">=5 juta":
                    SearchHarga();
                    break;
            }
        }catch (Exception e){
            Toast.makeText(getContext(), "Tidak ada internet", Toast.LENGTH_SHORT).show();
        }
    }

    private void SearchHarga(){
        ls = new ArrayList<>();
        addSmartphone(StaticList.getSamsung());
        addSmartphone(StaticList.getOppo());
        addSmartphone(StaticList.getAdvan());
        addSmartphone(StaticList.getAsus());
        addSmartphone(StaticList.getXiaomi());
        SwitchAdapter(ls);
    }

    private void addSmartphone(List<Smartphone> merk){
        for(Smartphone s : merk){
            if(s.getBobot_harga() == getPositionSpinner(spinnerKriteria)) ls.add(s);
        }
    }

    private void SwitchAdapter(List<Smartphone> smartphones){
        ViewAdapter viewAdapter = new ViewAdapter(smartphones);
        recyclerAdapter(viewRecycler, viewAdapter);
    }

}
