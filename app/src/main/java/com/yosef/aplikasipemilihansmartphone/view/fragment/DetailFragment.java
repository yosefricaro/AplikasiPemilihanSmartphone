package com.yosef.aplikasipemilihansmartphone.view.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.yosef.aplikasipemilihansmartphone.R;
import com.yosef.aplikasipemilihansmartphone.base.BaseFragment;
import com.yosef.aplikasipemilihansmartphone.model.Smartphone;
import com.yosef.aplikasipemilihansmartphone.model.StaticList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailFragment extends BaseFragment {

    @BindView(R.id.smartphone_image) ImageView image;
    @BindView(R.id.smartphone_tipe) TextView tipe;
    @BindView(R.id.smartphone_harga) TextView harga;
    @BindView(R.id.smartphone_rilis) TextView rilis;
    @BindView(R.id.smartphone_favorit) TextView txtFavorit;
    @BindView(R.id.detail_layar) TextView detailLayar;
    @BindView(R.id.detail_os) TextView detailOS;
    @BindView(R.id.detail_chipset) TextView detailChipset;
    @BindView(R.id.detail_processor) TextView detailProcessor;
    @BindView(R.id.detail_memori) TextView detailMemori;
    @BindView(R.id.detail_ram) TextView detailRAM;
    @BindView(R.id.detail_belakang) TextView detailBelakang;
    @BindView(R.id.detail_depan) TextView detailDepan;
    @BindView(R.id.detail_baterai) TextView detailBaterai;
    @BindView(R.id.button_lazada) Button buttonLazada;
    @BindView(R.id.button_tokopedia) Button buttonTokopedia;
    @BindView(R.id.button_shopee) Button buttonShopee;
    @BindView(R.id.button_blibli) Button buttonBlibli;
    @BindView(R.id.button_bukalapak) Button buttonBukalapak;
    @BindView(R.id.favorit) CheckBox favorit;
    boolean mProcessLike;
    int jmlFavorit;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_detail, container, false);
        ButterKnife.bind(this, rootView);

        Smartphone smartphone = StaticList.getSmartphone();

        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        DatabaseReference mDatabaseLike = FirebaseDatabase.getInstance().getReference().child("favorit");
        DatabaseReference mDatabaseSmartphone = FirebaseDatabase.getInstance().getReference().child("smartphone")
                .child(smartphone.getMerk().toLowerCase()).child(smartphone.getUid());

        mDatabaseLike.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.child(smartphone.getUid()).hasChild(mAuth.getCurrentUser().getUid())){
                    favorit.setChecked(true);
                } else{
                    favorit.setChecked(false);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("The read failed: " ,databaseError.getMessage());
            }
        });

        mDatabaseSmartphone.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                jmlFavorit = Integer.parseInt(dataSnapshot.child("favorit").getValue().toString());
                txtFavorit.setText(jmlFavorit + " favorit");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("The read failed: ", databaseError.getMessage());
            }
        });

        if (!TextUtils.isEmpty(smartphone.getImage_url()))
            Picasso.with(getContext())
                    .load(smartphone.getImage_url())
                    .placeholder(R.drawable.smartphone)
                    .error(R.drawable.smartphone)
                    .into(image);

        tipe.setText(smartphone.getTipe());
        harga.setText(smartphone.getHarga());
        rilis.setText(smartphone.getRilis());
        detailLayar.setText(smartphone.getLayar());
        detailOS.setText(smartphone.getOs());
        detailChipset.setText(smartphone.getChipset());
        detailProcessor.setText(smartphone.getProcessor());
        detailMemori.setText(smartphone.getMemori());
        detailRAM.setText(smartphone.getRam());
        detailBelakang.setText(smartphone.getKamerablk());
        detailDepan.setText(smartphone.getKameradpn());
        detailBaterai.setText(smartphone.getBaterai());

        favorit.setOnClickListener(view -> {
            mProcessLike = true;
            mDatabaseLike.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (mProcessLike) {
                        if (dataSnapshot.child(smartphone.getUid()).hasChild(mAuth.getCurrentUser().getUid())) {
                            mDatabaseLike.child(smartphone.getUid()).child(mAuth.getCurrentUser().getUid()).removeValue();
                            mDatabaseSmartphone.child("favorit").setValue(jmlFavorit - 1);
                            mProcessLike = false;
                        } else {
                            mDatabaseLike.child(smartphone.getUid()).child(mAuth.getCurrentUser().getUid()).setValue("RandomValue");
                            mDatabaseSmartphone.child("favorit").setValue(jmlFavorit + 1);
                            mProcessLike = false;
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e("The read failed: ", databaseError.getMessage());
                }
            });
        });

        buttonLazada.setOnClickListener(view -> {
            Uri webpage = Uri.parse("https://www.lazada.co.id/beli-handphone/?q="+smartphone.getTipe());
            eCommerce(webpage);
        });
        buttonTokopedia.setOnClickListener(view -> {
            Uri webpage = Uri.parse("https://www.tokopedia.com/search?st=product&q="+smartphone.getTipe()+"&sc=24&ob=8");
            eCommerce(webpage);
        });
        buttonShopee.setOnClickListener(view -> {
            Uri webpage = Uri.parse("http://www.shopee.co.id/search/?facet=%257B%252240%2522%253A%255B13%252C1211%255D%257D&keyword="+smartphone.getTipe()+"&page=0&sortBy=sales");
            eCommerce(webpage);
        });
        buttonBlibli.setOnClickListener(view -> {
            Uri webpage = Uri.parse("https://www.blibli.com/jual/"+smartphone.getTipe()+"?s="+smartphone.getTipe()+"&c=AN-1000001&sem=false&o=7&");
            eCommerce(webpage);
        });
        buttonBukalapak.setOnClickListener(view -> {
            Uri webpage = Uri.parse("https://www.bukalapak.com/c/handphone/hp-smartphone?utf8=%E2%9C%93&search%5Bkeywords%5D="+smartphone.getTipe()+"&search%5Bsort_by%5D=weekly_sales_ratio%3Adesc");
            eCommerce(webpage);
        });
        return rootView;
    }

    private void eCommerce(Uri webpage){
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivity(intent);
        }
    }

}
