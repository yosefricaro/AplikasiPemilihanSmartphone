package com.yosef.aplikasipemilihansmartphone.base;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.yosef.aplikasipemilihansmartphone.R;


/**
 * Created by Yosefricaro on 11/11/2017.
 */

public class BaseFragment extends Fragment {
    protected ArrayAdapter<CharSequence> setDropdown(int array){
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(), array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return adapter;
    }

    protected FragmentTransaction fragmentTransaction(Fragment fragment){
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, fragment);
        ft.commit();
        return ft;
    }

    protected String getItemSpinner(Spinner spinner){
        return spinner.getSelectedItem().toString();
    }

    protected int getPositionSpinner(Spinner spinner){
        return spinner.getSelectedItemPosition()+1;
    }

    protected String getText(EditText editText){
        return editText.getText().toString().trim();
    }

    protected void recyclerAdapter(RecyclerView recyclerView, RecyclerView.Adapter adapter){
        GridLayoutManager gridLayout = new GridLayoutManager(getContext(), 1);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(gridLayout);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setItemViewCacheSize(10);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerView.setAdapter(adapter);
    }
}
