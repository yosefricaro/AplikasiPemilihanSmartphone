package com.yosef.aplikasipemilihansmartphone.model;

/**
 * Created by Yosefricaro on 01/01/2018.
 */

public class User {
    private String email;
    private String nama;
    private String jk;
    private String pekerjaan;

    public String getEmail() {
        return email;
    }

    public String getNama() {
        return nama;
    }

    public String getJk() {
        return jk;
    }

    public String getPekerjaan() {
        return pekerjaan;
    }
}
