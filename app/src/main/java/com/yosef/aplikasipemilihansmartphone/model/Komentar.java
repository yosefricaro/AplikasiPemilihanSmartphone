package com.yosef.aplikasipemilihansmartphone.model;

/**
 * Created by Yosefricaro on 30/01/2018.
 */

public class Komentar {
    private String uid;
    private String uid_user;
    private String teks;
    private String tanggal;
    private String jam;

    public String getUid() {
        return uid;
    }

    public String getTeks() {
        return teks;
    }

    public String getTanggal() {
        return tanggal;
    }

    public String getJam() {
        return jam;
    }

    public String getUid_user() {
        return uid_user;
    }
}
