package com.yosef.aplikasipemilihansmartphone.model;

/**
 * Created by Yosefricaro on 09/11/2017.
 */

public class Smartphone {
    private String merk;
    private String tipe;
    private String image_url;
    private int bobot_harga;
    private int bobot_layar;
    private int bobot_processor;
    private int bobot_ram;
    private int bobot_memori;
    private int bobot_kamerablk;
    private int bobot_kameradpn;
    private int bobot_baterai;
    private String harga;
    private String rilis;
    private String layar;
    private String os;
    private String chipset;
    private String processor;
    private String ram;
    private String memori;
    private String kamerablk;
    private String kameradpn;
    private String baterai;
    private String uid;
    private int favorit;
    private double vektorV;
    private double vektorS;

    public String getTipe() {
        return tipe;
    }

    public int getBobot_harga() {
        return bobot_harga;
    }

    public int getBobot_processor() {
        return bobot_processor;
    }

    public int getBobot_ram() {
        return bobot_ram;
    }

    public int getBobot_memori() {
        return bobot_memori;
    }

    public int getBobot_kamerablk() {
        return bobot_kamerablk;
    }

    public int getBobot_kameradpn() {
        return bobot_kameradpn;
    }

    public int getBobot_baterai() {
        return bobot_baterai;
    }

    public String getHarga() {
        return harga;
    }

    public String getRilis() {
        return rilis;
    }

    public String getLayar() {
        return layar;
    }

    public String getOs() {
        return os;
    }

    public String getChipset() {
        return chipset;
    }

    public String getProcessor() {
        return processor;
    }

    public String getRam() {
        return ram;
    }

    public String getMemori() {
        return memori;
    }

    public String getKamerablk() {
        return kamerablk;
    }

    public String getKameradpn() {
        return kameradpn;
    }

    public String getBaterai() {
        return baterai;
    }

    public String getImage_url() {
        return image_url;
    }

    public double getVektorV() {
        return vektorV;
    }

    public void setVektorV(double vektorV) {
        this.vektorV = vektorV;
    }

    public double getVektorS() {
        return vektorS;
    }

    public void setVektorS(double vektorS) {
        this.vektorS = vektorS;
    }

    public int getBobot_layar() {
        return bobot_layar;
    }

    public String getMerk() {
        return merk;
    }

    public String getUid() {
        return uid;
    }

    public int getFavorit() {
        return favorit;
    }
}
