package com.yosef.aplikasipemilihansmartphone.model;

import java.util.List;

/**
 * Created by Yosefricaro on 09/11/2017.
 */

public class StaticList {
    private static List<Smartphone> samsung;
    private static List<Smartphone> oppo;
    private static List<Smartphone> advan;
    private static List<Smartphone> xiaomi;
    private static List<Smartphone> asus;
    private static Smartphone smartphone;
    private static List<Smartphone> result;
    private static User user;

    public static List<Smartphone> getSamsung() {
        return samsung;
    }

    public static void setSamsung(List<Smartphone> samsung) {
        StaticList.samsung = samsung;
    }

    public static List<Smartphone> getOppo() {
        return oppo;
    }

    public static void setOppo(List<Smartphone> oppo) {
        StaticList.oppo = oppo;
    }

    public static List<Smartphone> getAdvan() {
        return advan;
    }

    public static void setAdvan(List<Smartphone> advan) { StaticList.advan = advan; }

    public static List<Smartphone> getXiaomi() {
        return xiaomi;
    }

    public static void setXiaomi(List<Smartphone> xiaomi) {
        StaticList.xiaomi = xiaomi;
    }

    public static List<Smartphone> getAsus() {
        return asus;
    }

    public static void setAsus(List<Smartphone> asus) {
        StaticList.asus = asus;
    }

    public static Smartphone getSmartphone() { return smartphone; }

    public static void setSmartphone(Smartphone smartphone) {
        StaticList.smartphone = smartphone;
    }

    public static List<Smartphone> getResult() {
        return result;
    }

    public static void setResult(List<Smartphone> result) {
        StaticList.result = result;
    }

    public static User getUser() {
        return user;
    }

    public static void setUser(User user) {
        StaticList.user = user;
    }
}
